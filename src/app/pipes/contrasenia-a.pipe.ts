import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contraseniaA'
})
export class ContraseniaAPipe implements PipeTransform {

  transform(value: string, activar_desactivar: boolean): string {
    if (activar_desactivar) {
      return "*".repeat(value.length);
    }
    return value;
  }

}
