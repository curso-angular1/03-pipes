import { Component } from '@angular/core';
import { promise } from 'protractor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombre: string = 'Capitán América';

  arreglo: number[] = [1, 2, 3, 4, 5, 6, 7, 8];

  PI: number = Math.PI;
  porcetaje: number = 0.234;

  salario: number = 254.456;

  heroe = {
    nombre: 'logan',
    clave: 'wolverine',
    edad: 32,
    direccion: 'calle 1 casa blanca'
  }

  valorPromesa = new Promise<string>((resolve, reject) => {
    setTimeout(() => {
      resolve("hola mundo");
    }, 4500);
  })

  fecha: Date = new Date();

  idioma: string = 'es-MX';

  nombre2: string = "RaFAel dE jESús"

  videoUrl: string = 'https://www.youtube.com/embed/lWgvuOuZHfg'

  contrasenia: string = "miContrasenia"
  activar_desactivar_contrasenia: boolean = true;

}
