import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import '@angular/common/locales/global/es-MX';
import '@angular/common/locales/global/fr';
import { CapitalidoPipe } from './pipes/capitalido.pipe';
import { DomseguroPipe } from './pipes/domseguro.pipe';
import { ContraseniaAPipe } from './pipes/contrasenia-a.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CapitalidoPipe,
    DomseguroPipe,
    ContraseniaAPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es-MX'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
